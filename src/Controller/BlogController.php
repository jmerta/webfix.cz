<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController {

    /**
     * @Route("/blog", name="blog")
     */
    public function listAction(Request $request)
    {

        $repo = $this->getDoctrine()->getRepository(Article::class);

        $data = $repo->findBy([], ['updatedAt' => 'DESC']);

        // replace this example code with whatever you need
        return $this->render('web/blog.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'articles' => $data
        ]);
    }

    /**
     * @Route("/blog/{url}", name="article")
     * @param string  $url
     * @param Request $request
     * @return Response
     */
    public function detailAction(string $url, Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);

        $data = $repo->findOneBy(['url' => $url]);

        // replace this example code with whatever you need
        return $this->render('web/article.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'article' => $data
        ]);
    }
}
