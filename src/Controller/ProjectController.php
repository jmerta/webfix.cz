<?php

namespace App\Controller;

use App\Entity\Project;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController {

    /**
     * @Route("/projekty", name="projekty")
     */
    public function listAction(Request $request)
    {
        /** @var $repo EntityRepository */
        $repo = $this->getDoctrine()->getRepository(Project::class);

        $data = $repo->findBy([], ['updatedAt' => 'DESC']);

        // replace this example code with whatever you need
        return $this->render('web/projects.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'projects' => $data
        ]);
    }

    /**
     * @Route("/projekty/{url}", name="projekt")
     * @param string  $url
     * @param Request $request
     * @return Response
     */
    public function detailAction(string $url, Request $request)
    {
        /** @var $repo EntityRepository */
        $repo = $this->getDoctrine()->getRepository(Project::class);

        $data = $repo->findOneBy(['url' => $url]);

        return $this->render('web/project.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'project' => $data
        ]);
    }
}
